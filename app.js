document.querySelectorAll('[data-toggle="modal"]').forEach(el => {
  el.addEventListener('click', () => {
    const target = el.getAttribute('data-target');
    document.getElementById(target).classList.add('open');
  })
});

function closeModals() {
  document.querySelectorAll('.modal').forEach(el => {
    el.classList.remove('open');
    const video = el.querySelector('iframe');

    // Refresh the iframe `src` attribute, causing it to reload and thus stop the video from playing in the background
    // and using data for nothing
    video.setAttribute('src', video.getAttribute('src'))
  });
}

document.querySelectorAll('[data-toggle="close-modal"]').forEach(el => {
  el.addEventListener('click', closeModals);
});

document.addEventListener('click', e => {
  if (e.target.classList.contains('modal')) {
    closeModals();
  }
});

document.querySelector('#contact-form').addEventListener('submit', e => {
  e.preventDefault();

  const fieldName = document.querySelector('#contact-name');
  const fieldFrom = document.querySelector('#contact-email');
  const fieldType = document.querySelector('#contact-type');
  const fieldSubject = document.querySelector('#contact-subject');
  const fieldMessage = document.querySelector('#contact-message');

  const name = fieldName.value;
  const from = fieldFrom.value;
  const type = fieldType.value;
  const subject = fieldSubject.value;
  const message = fieldMessage.value;

  fetch('/contact.php', {
    method: 'POST',
    cache: 'no-cache',
    redirect: 'follow',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
    },
    body: JSON.stringify({
      name,
      from,
      type,
      subject,
      message
    })
  }).then(res => {
    if (res.status !== 200) {
      alert('Erreur lors de l\'envoi de votre message ☹\nVeuillez réessayer.');
      return;
    }

    fieldName.value = '';
    fieldFrom.value = '';
    fieldType.value = 'Autres';
    fieldSubject.value = '';
    fieldMessage.value = '';
    alert('Votre message a bien été envoyé 🍇');
  }).catch(err => {
    console.log('Error 🤔');
    console.log(err);
  });
});
