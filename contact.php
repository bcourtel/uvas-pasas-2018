<?php
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    header('HTTP/1.1 405 Method Not Allowed');
    die();
}

$authorizedTypes = [
    'Autres',
    'Communication',
    'Programmation',
    'Partenariat'
];

$contactEmail = 'contact@uvaspasas.fr';

$data = json_decode(trim(file_get_contents("php://input")), true);

$from = $data['from'];
$name = $data['name'];
$subject = $data['subject'];
$type = $data['type'];
$message = $data['message'];

if (empty($from) || empty($subject) || empty($type) || empty($message) || empty($name)
    || !filter_var($from, FILTER_VALIDATE_EMAIL)
    || !in_array($type, $authorizedTypes)) {
    header('HTTP/1.1 400 Bad Request');
    die('Bad request');
}

$headers = 'From: ' . $name . ' <' . $from . ">\r\n" .
           'Sender: ' . $contactEmail . "\r\n" .
           'Reply-To: ' . $name . ' <' . $from . ">\r\n" .
           'X-Mailer: PHP/' . phpversion();

switch($type) {
	case 'Programmation':
		$to = 'programmation.uvaspasas@gmail.com';
	break;

	case 'Communication':
	    $to = 'communication.uvaspasas@gmail.com';
	break;

	default:
	    if ($type !== 'Autres') {
	        $subject = '[' . $type . '] ' . $subject;
	    }

		$to = 'festival.uvaspasas@gmail.com';
	break;
}

mail($to, $subject, $message, $headers);
