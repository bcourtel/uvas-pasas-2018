#!/usr/bin/env bash

rm -rf ./dist
mkdir ./dist
cp -rf img ./dist/img
cp * ./dist/
cp ./.htaccess ./dist/
rm dist/build.sh
